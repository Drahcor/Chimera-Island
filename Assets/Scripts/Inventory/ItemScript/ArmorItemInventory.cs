using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Armor Item")]
public class ArmorItemInventory : InventoryItem
{
    public override void AssignItemToPlayer(PlayerEquipement playerEquipement)
    {
        playerEquipement.AssignArmorItem(this);
    }
}
