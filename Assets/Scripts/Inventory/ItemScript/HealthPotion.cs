using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Health Potion Item")]
public class HealthPotion : InventoryItem
{
    [SerializeField] private int healthPoints;
    public override void AssignItemToPlayer(PlayerEquipement playerEquipement)
    {
        playerEquipement.AssignHeathPotionItem(this);
    }
    public int GetHealthPoints()
    {
        return healthPoints;
    }
}
