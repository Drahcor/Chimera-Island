using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Hand { LEFT ,RIGHT}

[CreateAssetMenu (menuName = "Scriptable Objects/Inventory System/Items/Hand Item")]
public class HandItemInventory : InventoryItem
{
    public Hand hand;

    public override void AssignItemToPlayer(PlayerEquipement playerEquipement)
    {
        playerEquipement.AssignHandItem(this);
    }
}
