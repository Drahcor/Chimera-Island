using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Helmets")]
public class HelmetInventoryItem : InventoryItem
{
    public override void AssignItemToPlayer(PlayerEquipement playerEquipement)
    {
        playerEquipement.AssignHelmetItem(this);
    }
}
