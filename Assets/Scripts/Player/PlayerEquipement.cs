using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipement : MonoBehaviour
{
    [SerializeField] private Inventory inventory;
    [SerializeField] private Transform inventoryUIParent;

    [SerializeField] private Transform headnAnchor;
    [SerializeField] private Transform leftAnchor;

    [SerializeField] private Transform rightAnchor;
    [SerializeField] private Transform armorAnchor;

    [Header ("Anchors")]
    private GameObject currentHeadOBJ;
    private GameObject currentLeftOBJ;
    private GameObject currentRightOBJ;
    private GameObject currentArmorOBJ;

    private int playerHealth = 3;

    private void Start()
    {
        inventory.InitInventory(this);
        inventory.OpenInventoryUI();
    }
    public void AssignHelmetItem(HelmetInventoryItem item)
    {
        DestroyIfNotNull(currentHeadOBJ);
        currentHeadOBJ = CreateNewItemInstance(item, headnAnchor);
    }
    public void AssignHandItem(HandItemInventory item)
    {
        switch (item.hand)
        {
            case Hand.LEFT:
                DestroyIfNotNull(currentLeftOBJ);
                currentLeftOBJ = CreateNewItemInstance(item, leftAnchor);
                break;
            case Hand.RIGHT:
                DestroyIfNotNull(currentRightOBJ);
                currentRightOBJ = CreateNewItemInstance(item, rightAnchor);
                break;
            default:
                break;
        }
    }
    internal void AssignHeathPotionItem(HealthPotion healthPotion)
    {
        inventory.RemoveItem(healthPotion, 1);

        if (playerHealth <= 20)
            playerHealth += healthPotion.GetHealthPoints();
        if (playerHealth >= 20)
            playerHealth = 20;

        Debug.Log(string.Format("Player has now {0} healthPoints", playerHealth));
    }
    public void AssignArmorItem(ArmorItemInventory item)
    {
        DestroyIfNotNull(currentArmorOBJ);
        currentArmorOBJ = CreateNewItemInstance(item, armorAnchor);
    }

    private GameObject CreateNewItemInstance(InventoryItem item, Transform anchor)
    {
        var itemInstance = Instantiate(item.GetPrefab(), anchor);
        itemInstance.transform.localPosition = item.GetLocalPosition();
        itemInstance.transform.localRotation = item.GetLocalRotation();
        return itemInstance;
    }
    private void DestroyIfNotNull(GameObject obj)
    {
        if (obj)
            Destroy(obj);
    }

    public Transform GetUIParent()
    {
        return inventoryUIParent;
    }    
}