using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private CharacterController controller;
    public Transform cam;

    public float speed = 12f;

    public float turnSmooth = 0.1f;
    float turnSmoothVelocity;

    public Transform target;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if(direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) *Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmooth);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDir.normalized * speed * Time.deltaTime);
        }

        // Lock ennemi
        if(target != null && Input.GetKeyDown(KeyCode.Tab))
        {
            Camera.main.GetComponent<Cinemachine.CinemachineFreeLook>().LookAt = target;
        }
        else
        {
            Camera.main.GetComponent<Cinemachine.CinemachineFreeLook>().LookAt = this.gameObject.transform;
        }
    }
}