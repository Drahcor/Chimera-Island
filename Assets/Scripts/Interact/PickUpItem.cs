using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : Interactable
{
	public Inventory inventory;
	public HealthPotion potion;
	public InventoryItem baton;
	public InventoryItem caillou;
	public InventoryItem fruit;

	// When the player interacts with the item
	public override void Interact()
	{
		base.Interact();
		PickUp();
	}

	// Pick up the item
	void PickUp()
	{
		// v�rifie quel OBJ ajouter � la liste
		switch (tagCollider)
		{
			case "soin":
				inventory.AddItem(potion, 1);
				break;
			case "baton":
				inventory.AddItem(baton, 1);
				break;
			case "caillou":
				inventory.AddItem(caillou, 1);
				break;
			case "fruit":
				inventory.AddItem(fruit, 1);
				break;
			default:
				break;
		}
		Destroy(gameObject);    // Destroy item from scene
	}
}