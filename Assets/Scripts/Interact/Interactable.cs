using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;

    public bool hasInteracted;

    public Transform player;
    public Transform interactionTransform;
    public string tagCollider;

    public void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();
        interactionTransform = this.transform;
    }

    public virtual void Interact()
    {
        if (hasInteracted)
        {
          Debug.Log("interact with" + transform.name);
          tagCollider = transform.GetComponent<BoxCollider>().tag;
        }


        //Destroy(gameObject);
    }

    void Update()
    {
        float distance = Vector3.Distance(player.position, interactionTransform.position);

        if(distance <= radius)
        {
            hasInteracted = true;
            Interact();
        }
        else
            hasInteracted = false;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position,radius);
    }
}